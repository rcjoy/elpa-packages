;;; Generated package description from company-org-block.el  -*- no-byte-compile: t -*-
(define-package "company-org-block" "20220809.2027" "Org blocks company backend" '((emacs "25.1") (company "0.8.0") (org "9.2.0")) :commit "29a2edb35e18c2627dcfa0641852a55d9639263c" :authors '(("Alvaro Ramirez")) :maintainer '("Alvaro Ramirez") :url "https://github.com/xenodium/company-org-block")
