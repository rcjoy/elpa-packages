(define-package "modus-themes" "20221129.211" "Elegant, highly legible and customizable themes"
  '((emacs "27.1"))
  :commit "18dd2457d233859ad4d102d797d6744525177ac4" :authors
  '(("Protesilaos Stavrou" . "info@protesilaos.com"))
  :maintainer
  '("Modus-Themes Development" . "~protesilaos/modus-themes@lists.sr.ht")
  :keywords
  '("faces" "theme" "accessibility")
  :url "https://git.sr.ht/~protesilaos/modus-themes")
;; Local Variables:
;; no-byte-compile: t
;; End:
