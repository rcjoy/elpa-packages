(define-package "buttercup" "20221026.2006" "Behavior-Driven Emacs Lisp Testing"
  '((emacs "24.3"))
  :commit "eaa4b3ccd115a2bb25be98dc637950645d3adbae" :authors
  '(("Jorgen Schaefer" . "contact@jorgenschaefer.de"))
  :maintainer
  '("Ola Nilsson" . "ola.nilsson@gmail.com")
  :url "https://github.com/jorgenschaefer/emacs-buttercup")
;; Local Variables:
;; no-byte-compile: t
;; End:
