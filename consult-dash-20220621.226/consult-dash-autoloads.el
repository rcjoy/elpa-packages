;;; consult-dash-autoloads.el --- automatically extracted autoloads  -*- lexical-binding: t -*-
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "consult-dash" "consult-dash.el" (0 0 0 0))
;;; Generated autoloads from consult-dash.el

(autoload 'consult-dash "consult-dash" "\
Consult interface for dash documentation.

INITIAL is the default value provided.

\(fn &optional INITIAL)" t nil)

(register-definition-prefixes "consult-dash" '("consult-dash-"))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; consult-dash-autoloads.el ends here
