(define-package "slime" "20221127.2251" "Superior Lisp Interaction Mode for Emacs"
  '((cl-lib "0.5")
    (macrostep "0.9"))
  :commit "ba29269074534bd66758bd0a67082378e2587abe" :keywords
  '("languages" "lisp" "slime")
  :url "https://github.com/slime/slime")
;; Local Variables:
;; no-byte-compile: t
;; End:
