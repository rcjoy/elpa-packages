(define-package "consult" "20221201.744" "Consulting completing-read"
  '((emacs "27.1")
    (compat "28.1"))
  :commit "016a92c513c0cd88507a11eac08a004c5e96bba8" :authors
  '(("Daniel Mendler and Consult contributors"))
  :maintainer
  '("Daniel Mendler" . "mail@daniel-mendler.de")
  :url "https://github.com/minad/consult")
;; Local Variables:
;; no-byte-compile: t
;; End:
