;;; -*- no-byte-compile: t -*-
(define-package "jiralib2" "20200520.2031" "JIRA REST API bindings to Elisp" '((emacs "25") (request "0.3") (dash "2.14.1")) :commit "c21c4e759eff549dbda11099f2f680b78d7f5a01" :keywords '("comm" "jira" "rest" "api") :authors '(("Henrik Nyman" . "h@nyymanni.com")) :maintainer '("Henrik Nyman" . "h@nyymanni.com") :url "https://github.com/nyyManni/jiralib2")
