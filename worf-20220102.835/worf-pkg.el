(define-package "worf" "20220102.835" "A warrior does not press so many keys! (in org-mode)"
  '((swiper "0.11.0")
    (ace-link "0.1.0")
    (hydra "0.13.0")
    (zoutline "0.1.0"))
  :commit "8681241e118585824cd256e5b026978bf06c7e58" :authors
  '(("Oleh Krehel" . "ohwoeowho@gmail.com"))
  :maintainer
  '("Oleh Krehel" . "ohwoeowho@gmail.com")
  :keywords
  '("lisp")
  :url "https://github.com/abo-abo/worf")
;; Local Variables:
;; no-byte-compile: t
;; End:
