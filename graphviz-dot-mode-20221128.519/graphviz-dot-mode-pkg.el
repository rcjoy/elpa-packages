(define-package "graphviz-dot-mode" "20221128.519" "Mode for the dot-language used by graphviz (att)."
  '((emacs "25.0"))
  :commit "46afe4825b4a3a5d1a1ac65ec7f585051f756b78" :maintainer
  '("Pieter Pareit" . "pieter.pareit@gmail.com")
  :keywords
  '("mode" "dot" "dot-language" "dotlanguage" "graphviz" "graphs" "att")
  :url "https://ppareit.github.io/graphviz-dot-mode/")
;; Local Variables:
;; no-byte-compile: t
;; End:
