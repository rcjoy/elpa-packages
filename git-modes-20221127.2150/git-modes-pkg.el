(define-package "git-modes" "20221127.2150" "Major modes for editing Git configuration files"
  '((emacs "25.1"))
  :commit "be96ef14fab6a2d76cca3ebf9a15b462a695923d" :authors
  '(("Sebastian Wiesner" . "lunaryorn@gmail.com")
    ("Rüdiger Sonderfeld" . "ruediger@c-plusplus.net")
    ("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainer
  '("Jonas Bernoulli" . "jonas@bernoul.li")
  :keywords
  '("convenience" "vc" "git")
  :url "https://github.com/magit/git-modes")
;; Local Variables:
;; no-byte-compile: t
;; End:
