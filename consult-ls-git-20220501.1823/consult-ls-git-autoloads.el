;;; consult-ls-git-autoloads.el --- automatically extracted autoloads  -*- lexical-binding: t -*-
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "consult-ls-git" "consult-ls-git.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from consult-ls-git.el

(autoload 'consult-ls-git "consult-ls-git" "\
Create a multi view for current git repository." t nil)

(autoload 'consult-ls-git-ls-files "consult-ls-git" "\
Select a tracked file from a git repository." t nil)

(autoload 'consult-ls-git-ls-files-other-window "consult-ls-git" "\
Select a tracked file from a git repository and open it in another window." t nil)

(autoload 'consult-ls-git-ls-status "consult-ls-git" "\
Select a file from a git repository considered to be modified or untracked.

Untracked files are only included if `consult-ls-git-show-untracked-files' is t." t nil)

(autoload 'consult-ls-git-ls-status-other-window "consult-ls-git" "\
Open a modified/untracked file from a git repository in another window.

Untracked files are only included if
`consult-ls-git-show-untracked-files' is t." t nil)

(autoload 'consult-ls-git-ls-stash "consult-ls-git" "\
Select a stash from a git repository and apply, pop or drop it." t nil)

(register-definition-prefixes "consult-ls-git" '("consult-ls-git-"))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; consult-ls-git-autoloads.el ends here
