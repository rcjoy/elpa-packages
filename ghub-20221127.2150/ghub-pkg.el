(define-package "ghub" "20221127.2150" "Client libraries for Git forge APIs."
  '((emacs "25.1")
    (compat "28.1.1.0")
    (let-alist "1.0.6")
    (treepy "0.1.1"))
  :commit "141987e62dc908f54a6ef0b7847ae5296591956b" :authors
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainer
  '("Jonas Bernoulli" . "jonas@bernoul.li")
  :keywords
  '("tools")
  :url "https://github.com/magit/ghub")
;; Local Variables:
;; no-byte-compile: t
;; End:
