(define-package "lispy" "20221114.2013" "vi-like Paredit"
  '((emacs "24.3")
    (ace-window "0.9.0")
    (iedit "0.9.9")
    (swiper "0.13.4")
    (hydra "0.14.0")
    (zoutline "0.2.0"))
  :commit "f35eadf8c1be43a395e196463314b17ea3b4e16f" :authors
  '(("Oleh Krehel" . "ohwoeowho@gmail.com"))
  :maintainer
  '("Oleh Krehel" . "ohwoeowho@gmail.com")
  :keywords
  '("lisp")
  :url "https://github.com/abo-abo/lispy")
;; Local Variables:
;; no-byte-compile: t
;; End:
