(define-package "worf" "20210702.1200" "A warrior does not press so many keys! (in org-mode)"
  '((swiper "0.11.0")
    (ace-link "0.1.0")
    (hydra "0.13.0")
    (zoutline "0.1.0"))
  :commit "28d381e2603a79340a94a410acbbb8a6b3e237d8" :authors
  '(("Oleh Krehel" . "ohwoeowho@gmail.com"))
  :maintainer
  '("Oleh Krehel" . "ohwoeowho@gmail.com")
  :keywords
  '("lisp")
  :url "https://github.com/abo-abo/worf")
;; Local Variables:
;; no-byte-compile: t
;; End:
