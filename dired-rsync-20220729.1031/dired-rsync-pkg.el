(define-package "dired-rsync" "20220729.1031" "Allow rsync from dired buffers"
  '((s "1.12.0")
    (dash "2.0.0")
    (emacs "24"))
  :commit "4057a36440988769bff0a859c347a19973b3ae62" :authors
  '(("Alex Bennée" . "alex@bennee.com"))
  :maintainer
  '("Alex Bennée" . "alex@bennee.com")
  :url "https://github.com/stsquad/dired-rsync")
;; Local Variables:
;; no-byte-compile: t
;; End:
